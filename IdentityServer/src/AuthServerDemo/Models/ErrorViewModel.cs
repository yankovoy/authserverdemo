﻿using IdentityServer4.Models;

namespace AuthServerDemo.Models
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}