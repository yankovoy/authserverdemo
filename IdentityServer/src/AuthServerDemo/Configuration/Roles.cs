﻿namespace AuthServerDemo.Configuration
{
    public static class Roles
    {
        public const string User = "user";
        public const string Admin = "admin";
    }
}
