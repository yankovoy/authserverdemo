# Authorization server based on IdentityServer4 #

In order to run this application you need navigate to: ..\IdentityServer\src\AuthServerDemo and execute **dotnet run**. If database settings MigrateOnStartup:true, then application with try connect to database (PostgreSQL) using specified connection string and apply all required migrations, and if MigrateOnStatupWithTestingData:true then app will add some fake data.

By default application going to listen http://localhost:5000

IdentityServer4 discovery document

**GET** ../.well-known/openid-configuration


```
#!json

{
  "issuer": "http://localhost:5000",
  "jwks_uri": "http://localhost:5000/.well-known/openid-configuration/jwks",
  "authorization_endpoint": "http://localhost:5000/connect/authorize",
  "token_endpoint": "http://localhost:5000/connect/token",
  "userinfo_endpoint": "http://localhost:5000/connect/userinfo",
  "end_session_endpoint": "http://localhost:5000/connect/endsession",
  "check_session_iframe": "http://localhost:5000/connect/checksession",
  "revocation_endpoint": "http://localhost:5000/connect/revocation",
  "introspection_endpoint": "http://localhost:5000/connect/introspect",
  "frontchannel_logout_supported": true,
  "frontchannel_logout_session_supported": true,
  "scopes_supported": [
    "openid",
    "profile",
    "email",
    "userscope",
    "api1",
    "users",
    "userscope",
    "offline_access"
  ],
  "claims_supported": [
    "sub",
    "picture",
    "nickname",
    "middle_name",
    "given_name",
    "family_name",
    "name",
    "updated_at",
    "locale",
    "zoneinfo",
    "birthdate",
    "gender",
    "website",
    "profile",
    "preferred_username",
    "email_verified",
    "email",
    "role",
    "user",
    "admin"
  ],
  "response_types_supported": [
    "code",
    "token",
    "id_token",
    "id_token token",
    "code id_token",
    "code token",
    "code id_token token"
  ],
  "response_modes_supported": [
    "form_post",
    "query",
    "fragment"
  ],
  "grant_types_supported": [
    "authorization_code",
    "client_credentials",
    "refresh_token",
    "implicit",
    "password"
  ],
  "subject_types_supported": [
    "public"
  ],
  "id_token_signing_alg_values_supported": [
    "RS256"
  ],
  "token_endpoint_auth_methods_supported": [
    "client_secret_basic",
    "client_secret_post"
  ],
  "code_challenge_methods_supported": [
    "plain",
    "S256"
  ]
}
```


### appsetings.json file contains configuration settings ###
**This settings is only for development purposes. Settings should be stored securely** 

* ConnectionStrings - to specify database connection string (for this demo PostgreSQL)
* DatabaseSettings - allow to migrate database context to latest version and add fake data into database, and define which connection string should be used
* Facebook - application key and secret for authorization
* ServerAuthentication - to define trusted authority. In this specific case system will issue JWT to itself and use to authorize users 


### UserProfileController ###
API ../api/profile provides GET\POST\PUT\DELETE methods to manage users profile. 

User should be authorized to access this endpoint

**POST** ../connect/token

Content-Type:application/x-www-form-urlencoded
Accept:application/json

client_id:demo

grant_type:password

username:admin@mail.com

password:qweQWE123!

client_secret:secret

scope:users

*RESPONSE*

```
#!json

{
"access_token":"eyJhbGciOiJSUzI1NiIsImtpZCI6IjVjNzZiNjI3ZjhiNDg1YjJkZTg4MmVjOWIwYTg2MmQwIiwidHlwIjoiSldUIn0.eyJuYmYiOjE0ODQzMTgxMTksImV4cCI6MTQ4NDMyMTcxOSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo1MDAwIiwiYXVkIjpbImh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC9yZXNvdXJjZXMiLCJ1c2VycyJdLCJjbGllbnRfaWQiOiJkZW1vIiwic3ViIjoiMSIsImF1dGhfdGltZSI6MTQ4NDMxODExOSwiaWRwIjoibG9jYWwiLCJuYW1lIjoiYWRtaW5AbWFpbC5jb20iLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6ImRjNzA4YzQyLTk0ZjgtNGUzOS04ZGQ0LTM1NmI5YzViMjJkNiIsImdpdmVuX25hbWUiOiJhZG1pbiIsImFkZHJlc3MiOiIxMiB3aWRlIHN0cmVldCwgbW9vbiIsImZhbWlseV9uYW1lIjoidWJlciIsInJvbGUiOlsiYWRtaW4iLCJ1c2VyIl0sImVtYWlsIjoiYWRtaW5AbWFpbC5jb20iLCJzY29wZSI6WyJ1c2VycyJdLCJhbXIiOlsicHdkIl19.LOTqgyr3j2ke1B2GrQaRyAJxXGky6oIi43uAId14rfHArYeKUPRtSqlyuOPSk6RTh7OZoaZ2aIP7v2qfFVGnpuwNLRVDEtjIcTMHvTVS7Kh_SWfKpS2INs1YPb6vWLlbOAz9Xwy9-nSXc47rGfsr_7mXpzyTexe1Gh06n_2ZYAyago4c6_Ziu7-mTzZXSJlg0j4PD40M8QANhfwzQQFTRn-yaop4WYJTxiyZTg931DsguSJLxAIoQol3shQub9s-aPMQKMVq4mlw9kABungWUbrkZZfUiAt7uJjGNuYh5kKgmYQZyEHGmcAR3WX3N2B69xrqEkQcOf4OxsA_hzmRqA",
"expires_in":3600,
"token_type":"Bearer"}
```

Authorized user:

**GET** ../api/profile?email={email}
*{email} optional parameter*

Authorized user in admin role:

**POST** ../api/profile

Content-Type:application/json

Authorization:Bearer XYS

```
#!json
{
	"email": "user email",
	"password": "passwodr",
	"firstname": "first name",
	"lastname": "last name",
	"address": "some address",
	"isAdmin": true|false
}
```

Authorized user:

**PUT** ../api/profile?email={email}

```
#!json
{
	"firstname": "first name",
	"lastname": "last name",
	"address": "some address",
}
```

Authorized user in admin role:

**DELETE** ../api/profile?email={email}